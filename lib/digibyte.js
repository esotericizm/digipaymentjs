const bitcore = require('bitcore-lib');
const digibyte = require('bitcoin');
const Promise = require('bluebird');
const request = require('request-promise');

const client = Promise.promisifyAll(new digibyte.Client({
  host: 'localhost',
  port: 14022,
  user: 'user',
  pass: 'password',
  timeout: 30000,
}));


module.exports = {

/**
 * Get a New Address from the wallet
 */
  getNewAddress() {
    return client.getNewAddressAsync();
  },

/**
 * Get a users balance
 * @param {string} addr
 * @returns {number}
 */
  getBalance(addr) {
    return request(`https://digiexplorer.info/api/addr/${addr}/balance`, { json: true });
  },

  /**
   * Get the wallet Balance
   * @returns {number} balance
   */
  getWalletBalance() {
    return client.getBalanceAsync();
  },

/**
 * Get A Users UTXO's
 * @param {string} addr
 * @returns {array}
 */
  getUserUtxos(addr) {
    return request(`https://digiexplorer.info/api/addr/${addr}/utxo`, { json: true });
  },

/**
 * Get Our Wallet Utxos
 * @returns {array}
 */
  getWalletUtxos() {
    return client.listUnspentAsync();
  },

/**
 * Dumps A private Key from the wallet
 * @param {string} addr
 * @returns {string}
 */
  getPrivateKey(addr) {
    return client.dumpPrivKeyAsync(addr);
  },

/**
 * Dumps array of private Keys
 * @param {array} addrs
 * @returns {array}
 */
  getPrivateKeys(addrs) {
    return Promise.map(addrs, (addr) => {
      return this.getPrivateKey(addr);
    });
  },

/**
 * Converts utxos to an array of addresses
 * @param {array} utxos
 * @returns {array}
 */
  utxoToAddressArray(utxos) {
    return Promise.map(utxos, (utxo) => {
      return utxo.address;
    });
  },

/**
 * Sets the transaction recipients
 * @param {object} tx
 * @param {array} addrs
 */
  addTransactionRecipients(tx, addrs) {
    return Promise.map(addrs, (addr) => {
      tx.to(addr, 100000000);
    })
    .then(() => {
      return tx;
    });
  },

/**
 * Sign a transaction with our wallet
 * @param {object} tx
 * @param {array} addrs
 * @returns {object}
 */
  signTransactionFromWallet(tx, addrs) {
    return this.getPrivateKeys(addrs)
      .then((privKeys) => {
        tx.sign(privKeys);
        return tx;
      });
  },

/**
 * Creates a new transaction from our wallet
 * @param {array} utxo
 * @param {array} addrs
 * @param {string} op_return
 * @returns {object} tx
 */
  createNewWalletTransaction(utxo, addrs, hashData) {
    let addrsArray = addrs;
    if (!addrs.isArray) {
      addrsArray = [addrs];
    }
    return Promise.all([
      this.getNewAddress(),
      this.getWalletUtxos(),
    ])
      .spread((changeAddress, utxos) => {
        const tx = new bitcore.Transaction();
        tx.from(utxos);
        tx.change(changeAddress);
        if (hashData) {
          tx.addData(hashData);
        }
        return Promise.all([
          tx,
          this.utxoToAddressArray(utxos),
        ]);
      })
      .spread((tx, walletAddresses) => {
        return Promise.all([
          walletAddresses,
          this.addTransactionRecipients(tx, addrsArray),
        ]);
      })
      .spread((walletAddresses, tx) => {
        return this.signTransactionFromWallet(tx, walletAddresses);
      })
      .then((tx) => {
        return tx.serialize();
      });
  },

/**
 * Sends a raw transaction using our wallet
 * @param {string} tx
 */
  sendTransactionFromWallet(tx) {
    return client.sendRawTransactionAsync(tx);
  },

};
