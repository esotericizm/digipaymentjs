const assert = require('assert');
const chai = require('chai');
const expect = chai.expect;

const client = require('../lib/digibyte');

describe('Wallet', function () {
	let Address = '';
	let walletUtxos = null;
	let walletAddresses = null;
	let rawTx = null;

	describe('getNewAddress()', function () {
		it('should be able to make a new address', function (done) {
			return client.getNewAddress()
			.then(function(addr){
				expect(addr).to.be.a('string');
				expect(addr).to.have.lengthOf(34);

				Address = addr;
				done();
			})
			.catch(function(err){
				assert.ifError(err);
				done();
			})
		});
	});

	describe('getBalance()', function() {
		it('should be able to get a users balance', function(done) {
			return client.getBalance(Address)
			.then(function(balance){
				expect(balance).to.be.a('number');
				done();
			})
			.catch(function(err) {
				assert.ifError(err);
				done();
			});
		});
	});

	describe('getWalletBalance()', function() {
		it('should get our wallets balance', function(done) {
			return client.getWalletBalance()
			.then(function(balance){
				expect(balance).to.be.a('number');
				expect(balance).to.not.equal(0);
				done();
			})
			.catch(function(err) {
				assert.ifError(err);
				done();
			});
		});
	});

	describe('getUserUtxos()', function() {
		it('should be able to get a users utxos', function(done) {
			return client.getUserUtxos(Address)
			.then(function(utxos) {
				expect(utxos).to.be.a('array');
				done();
			})
			.catch(function(err) {
				assert.ifError(err);
				done();
			});
		});
	});

	describe('getWalletUtxos()', function() {
		it('should get our wallet utxos', function(done) {
			return client.getWalletUtxos()
			.then(function(utxos) {
				walletUtxos = utxos;
				expect(utxos).to.be.a('array');
				done();
			})
			.catch(function(err) {
				assert.ifError(err);
				done();
			});
		})
	});

	describe('utxoToAddressArray()', function() {
		it('should turn an array of utxos into an array of addresses', function(done) {
			return client.utxoToAddressArray(walletUtxos)
			.then(function(addresses) {
				walletAddresses = addresses;
				expect(addresses[0]).to.have.lengthOf(34);
				expect(addresses).to.be.a('array');
				expect(addresses).to.have.lengthOf(walletUtxos.length);
				done();
			})
			.catch(function(err){
				assert.ifError(err);
				done();
			});
		});
	});

	describe('getPrivateKey()', function() {
		it('should dump a private key', function(done) {
			return client.getPrivateKey(Address)
			.then(function(privKey) {
				expect(privKey).to.have.lengthOf(52);
				done();
			})
			.catch(function(err) {
				assert.ifError(err);
				done();
			});
		});
	});

	describe('getPrivateKeys()', function() {
		it('should dump an array of private keys', function(done) {
			return client.getPrivateKeys(walletAddresses)
			.then(function(privateKeys) {
				expect(privateKeys[0]).to.have.lengthOf(52);
				expect(privateKeys).to.be.a('array');
				expect(privateKeys).to.have.lengthOf(walletUtxos.length);
				done();
			})
			.catch(function(err) {
				assert.ifError(err);
				done();
			});
		});
	});

	describe('createNewWalletTransaction()', function() {
		it('should create a new transaction from our wallet', function(done) {
			return client.createNewWalletTransaction(walletUtxos, Address, 'lolz')
			.then(function(tx) {
				rawTx = tx;
				expect(tx).to.be.a('string');
				done();
			})
			.catch(function(err) {
				assert.ifError(err);
				done();
			});
		});
	});

	describe('sendTransactionFromWallet()', function() {
		it('it should send a raw transaction', function(done) {
			return client.sendTransactionFromWallet(rawTx)
			.then(function(resp) {
				txid = resp;
				expect(resp).to.be.a('string');
				expect(resp).to.have.lengthOf(64);
				done();
			})
			.catch(function(err) {
				assert.ifError(err);
				done();
			});
		});
	});

});