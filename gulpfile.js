const concat = require('gulp-concat');
const eslint = require('gulp-eslint');
const gulp = require('gulp');
const jsdoc = require('gulp-jsdoc3');
const mocha = require('gulp-mocha');

gulp.task('compile', function() {
    gulp.src(['lib/*.js'])
        .pipe(concat('payment.js'))
        .pipe(gulp.dest('dist/'))
});

gulp.task('lint', function() {
		return gulp.src(['lib/*.js'])
				.pipe(eslint())
				.pipe(eslint.format())
})

gulp.task('test', function() {
		gulp.src(['test/*.js'], {read: false})
				.pipe(mocha())
});

gulp.task('doc', function (cb) {
    gulp.src(['dist/payment.js'], {read: false})
        .pipe(jsdoc(cb));
});

gulp.task('watch', function() {

	return gulp.watch(['test/*.js'], function() {
			gulp.run('test');
	});
});

gulp.task('default', function() {
    gulp.run('lint', 'test', 'doc', 'compile');
});