# digipaymentjs [![Build Status](https://travis-ci.org/esotericizm/digipaymentjs.svg?branch=master)](https://travis-ci.org/esotericizm/digipaymentjs)

> My world-class module


## Install

```
$ npm install --save digipaymentjs
```


## Usage

```js
const digipaymentjs = require('digipaymentjs');

digipaymentjs('unicorns');
//=> 'unicorns & rainbows'
```


## API

### digipaymentjs(input, [options])

#### input

Type: `string`

Lorem ipsum.

#### options

##### foo

Type: `boolean`<br>
Default: `false`

Lorem ipsum.


## License

MIT © [Esotericizm](https://digibyte.co)
